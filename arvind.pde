import ddf.minim.*;
import ddf.minim.ugens.*;


// PARAMETERS
float speedY, speedX, maxspeed = 0;

Minim minim;
AudioOutput out;

float[] extendRaga(int notes[], int up, int down) {
  int numNotes = notes.length;
  float[] extendedNotes = new float[numNotes + up + down + 1];
  int totalNdx = 0;
  for(int i = 0; i < numNotes; i++) {    
    extendedNotes[down+i] = carnaticscale[totalNdx];
    totalNdx += notes[i];
  }
  extendedNotes[down+numNotes] = carnaticscale[0] * 2;
  for(int i = 0; i < down; i++) {
    int extendedNdx = down - i - 1;
    int notesNdx = numNotes - i - 1;
    float divisor = pow(2, ((float)notes[notesNdx])/12);
    extendedNotes[extendedNdx] = extendedNotes[extendedNdx+1]/divisor;
  }
  for(int i = 0; i < up; i++) {
    int extendedNdx = down + numNotes + i + 1;
    float multiplier = pow(2, ((float)notes[i])/12);
    extendedNotes[extendedNdx] = extendedNotes[extendedNdx - 1]*multiplier;
  }  
  return extendedNotes;
}


static float carnaticscale[] = new float[12];
static float carnaticMultiplier;

static void setupCarnaticScale() {
  carnaticMultiplier = pow(2, 1.0/12);
  carnaticscale[0] = 400.0;
  println("carnaticmultiplier: " + carnaticMultiplier);
  for(int i=1; i < 12; i++) {
    carnaticscale[i] = carnaticscale[i-1] * carnaticMultiplier;
    println("i: " + i + "freq: " + carnaticscale[i]);  
  }

/*
  float sa  = carnaticscale[0];
  float ri1 = carnaticscale[1];
  float ri2 = carnaticscale[2];
  float ga1 = carnaticscale[3];
  float ga2 = carnaticscale[4];
  float ma1 = carnaticscale[5];
  float ma2 = carnaticscale[6];
  float pa  = carnaticscale[7];
  float da1 = carnaticscale[8];
  float da2 = carnaticscale[9];
  float ni1 = carnaticscale[10];
  float ni2 = carnaticscale[11];
*/
}

static int totalNotes = 0;
static float[] ragaNotes;
static int pixelsPerNote;

void setup() {
  size(480,600);
  

  setupCarnaticScale();
  int[] mohana = {2, 2, 3, 2, 3};
  int[] amritavarshini = {4, 2, 1, 4, 1};

  ragaNotes = extendRaga(amritavarshini, 3, 3); 
  totalNotes = ragaNotes.length;
  
  for(int i=0; i < ragaNotes.length; i++) {
    println("i: " + i + " freq: " + ragaNotes[i]);  
  }  
  println("height: " + height + " totalNotes: " + totalNotes);
  pixelsPerNote = height/totalNotes;
  println("pixelsPerNote: " + pixelsPerNote);
  
  minim = new Minim( this );
  out = minim.getLineOut( Minim.MONO, 2048 );
}

final float minus2pi(float angle) {
    if (angle >= 2*PI - 0.001) return angle - 2*PI;
    return angle;
}

void myarc(int x, int y, int dia, float startAngle,
           int speedNdxY, int speedNdxX) {
  strokeWeight(2);
  
  float angle = startAngle;

  int nArcs = floor(random(7)) + 2;
  
  for(int i = 0; i < nArcs; i++) {
    int newX = x + round(dia/2*cos(angle));
    int newY = y + round(dia/2*sin(angle));
    float newAngle = angle + PI;
    
    float arcAngle = minus2pi(newAngle);
    for(int j = 0; j < 3 + 4*speedNdxY; j++) {
      int green = round(random(255));
      int red = speedNdxX * 64 + round(random(63));
      int blue = (3 - speedNdxX) * 64 + round(random(63));
      //println("red: " + red + " blue: " + blue + " green: " + green);
      stroke(red, green, blue, 255);
      fill(red, green, blue, 255);
      arc(newX, newY, dia, dia, arcAngle, minus2pi(arcAngle + PI/36), PIE);
      arcAngle = minus2pi(arcAngle + 3*PI/36);
    }
    
    angle += 2*PI/nArcs;
    angle = minus2pi(angle);  
  } 
}

static int pTime = 0;
static boolean first = true;
static int totalElapsed = 0;
static int refreshElapsed = 0;
// in milliseconds
static int[] noteLengths = {500, 250, 125, 62};
static int pNoteLength = 1000;
static float startAngle = 0;

void draw() {
  int time = millis();
  int elapsed = time - pTime;
  pTime = time;
  float dist = abs(pmouseY - mouseY);
  
  speedY = dist*1000/elapsed;
  dist = abs(pmouseX - mouseX);
  speedX = dist*1000/elapsed;
  if (speedY >= 1000) { speedY = 999; }
  if (speedX >= 500) { speedX = 499; }
  int noteLengthNdx = floor(speedY/250);
  
  // max speed seems to be around 30000 median 10000
  if (speedY > maxspeed && !first) {
    maxspeed = speedY;
    println("dist: " + dist + " elapsed: " + elapsed);       
  }
  first = false;
  int noteNdx = mouseY/pixelsPerNote;
  if (noteNdx >= totalNotes)
    noteNdx = totalNotes - 1;
    
  int noteLength = noteLengths[noteLengthNdx];
  if (totalElapsed > pNoteLength) {
    totalElapsed = 0;
    pNoteLength = noteLength;
    float pitch = ragaNotes[noteNdx];
    println("speedX: " + speedX + " speedY: " + speedY + " noteLengthNdx: " + noteLengthNdx);
    //println("pTime: " + pTime + " noteNdx: " + noteNdx + 
     //       " freq: " + pitch + " noteLength: " + noteLength);
            
    out.playNote(0, ((float)noteLength)/1000, pitch);
  } else {
    totalElapsed += elapsed;
  }
     
  int keyWidth = 100;

  stroke(0);
  fill(0);
  rect(0, 0, keyWidth, height);
  
  stroke(#FFFFFF);
  strokeWeight(2);
  for(int i = 0; i < totalNotes; i++) {
    line(0, i * pixelsPerNote, keyWidth, i * pixelsPerNote); 
  }
  fill(#FFFFFF);
  rect(0, pixelsPerNote*noteNdx+1, keyWidth, pixelsPerNote-2);

  refreshElapsed += elapsed;
  if (refreshElapsed > 100) {
    refreshElapsed = 0;  
    fill(#FFFFFF);
    rect(keyWidth + 1, 0, width - keyWidth, height);
    //brush();
    myarc(mouseX, mouseY, 100, startAngle,
          noteLengthNdx, floor(speedX/125));
    startAngle += PI/6;
    startAngle = minus2pi(startAngle);
  }
}


